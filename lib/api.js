'use strict'

const request = require('request')
const qs = require('qs')
const crypto = require('crypto')

function generateHMAC(url, timestamp, apiKey, req) {
  const key = new Buffer(apiKey, 'hex')

  let data = req.method + url
  if (req.qs) {
    data += qs.stringify(req.qs)
  }
  data += timestamp
  if (req.body) {
    data += req.body
  }

  return crypto.createHmac('sha256', key)
    .update(data).digest('hex')
}

function defaultOnError(data) {
  console.error('[Error]', data)
}

function defaultLogger(data) {
  console.log(data)
}

function API(config) {
  this.logger = config.logger || {}
  this.logger.error = this.logger.error || defaultOnError
  this.logger.log = this.logger.log || defaultLogger

  if (!config.host || !config.name || !config.version) {
    throw new Error('These URL parameters are required: host, name, version')
  }

  if (!config.user || !config.apiKey) {
    throw new Error('These credential parameters are missing: user, apiKey')
  }

  this.user = config.user
  this.apiKey = config.apiKey
  this.format = config.format || 'json'
  this.debug = config.debug
  this.dryRun = config.dryRun
  this.apiBase = (config.protocol || 'http') + '://' + config.host + '/' + config.name + '/v' + config.version + '/'
}

API.prototype = {
  request(params) {
    const url = this.apiBase + params.endpoint
    this._log('debug', 'API base: ' + url)

    const timestamp = (new Date()).getTime()
    let contentType

    switch (this.format) {
      case 'json':
        contentType = 'application/json'
        break
      case 'xml':
        contentType = 'application/xml'
        break
      default:
        throw new Error(`Invalid format "${this.format}"`)
    }

    let data = {
      'method': params.method || 'GET',
      'uri': url,
      'headers': {
        'X-LLNW-Security-Principal': this.user,
        'X-LLNW-Security-Timestamp': timestamp,

        'Content-Type': contentType,
        'Accept': contentType
      }
    }

    if (data.method === 'GET') {
      data.qs = params.data || {}
    } else if (data.method === 'POST' || data.method === 'PUT') {
      data.body = params.data || ''
    }
    data.headers['X-LLNW-Security-Token'] = generateHMAC(url, timestamp, this.apiKey, data)

    const req = request(data)
    let body = ''
    let response

    req.on('data', (chunk) => {
      body += chunk.toString()
    })

    req.on('response', (r) => {
      response = r
    })

    let promise = new Promise((resolve, reject) => {
      if (this.dryRun) {
        resolve(data)
      } else {
        req.on('error', (err) => {
          this._log('error', err)
          reject(err)
        })
        req.on('end', (test) => {
          if (response.statusCode < 200 || response.statusCode >= 300) {
            body = body || response.body
            if (this.format === 'json') {
              body = JSON.parse(body)
            }
            this._log('error', body)
            reject(body)
          } else {
            if (body && this.format === 'json') {
              body = JSON.parse(body)
              this._log('debug', 'Response: ' + JSON.stringify(body, null, 4))
            } else {
              this._log('debug', 'Response: ' + body)
            }
            resolve(body)
          }
        })
        this._log('debug', 'Request headers: ' + JSON.stringify(req.headers, null, 4))
      }
    })

    return promise
  },

  _log(type, message) {
    if (type === 'error') {
      this.logger.error(message)
    } else if (type === 'debug' && this.debug) {
      this.logger.log(message)
    }
  }
}

module.exports = API
